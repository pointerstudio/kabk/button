using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveOnOffInstantiate : MonoBehaviour
{
    public GameObject position;
    public bool randomRotation = true;
    public GameObject original;
    private GameObject instance;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    void ReceiveMessage(string message)
    {
        if (message == "on") {
            instance = Instantiate(original, position.transform.position, Random.rotation);
        } else if (message == "off") {
            Destroy(instance);
        }
    }

    void ReceiveMessage(int message)
    {
        if (message == 1) {
            instance = Instantiate(original, position.transform.position, Random.rotation);
        } else if (message == 0) {
            Destroy(instance);
        }
    }
}
