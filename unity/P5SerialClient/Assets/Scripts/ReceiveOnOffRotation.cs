using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveOnOffRotation : MonoBehaviour
{
    public Vector3 rotation = new Vector3(1.0f, 1.0f, 1.0f);
    bool rotate = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (rotate) {
            gameObject.transform.Rotate(rotation);
        }
    }

    void ReceiveMessage(string message)
    {
        if (message == "on") {
            rotate = true;
        } else if (message == "off") {
            rotate = false;
        }
    }

    void ReceiveMessage(int message)
    {
        if (message == 1) {
            rotate = true;
        } else if (message == 0) {
            rotate = false;
        }
    }
}
