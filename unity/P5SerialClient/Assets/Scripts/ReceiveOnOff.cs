using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveOnOff : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void ReceiveMessage(string message)
    {
        if (message == "on") {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        } else if (message == "off") {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    void ReceiveMessage(int message)
    {
        if (message == 1) {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        } else if (message == 0) {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
