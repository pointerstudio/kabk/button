using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NativeWebSocket;

[Serializable]
public class P5SerialDataSerialPortOptions
{
}

[Serializable]
public class P5SerialDataSerialPort
{
    public string serialport;
    public P5SerialDataSerialPortOptions serialoptions;
}

[Serializable]
public class P5SerialMessage<T>
{
    public string method;
    public T data;
}

public class WebSocketConnection : MonoBehaviour
{
    [Header("Serial")]
    [Tooltip("Get this from Thonny at the bottom right")]
    public string serialPort = "/dev/ttyACM0";
    [Tooltip("You probably want this.")]
    public bool openPortAutomatically = true;
    [Header("Websocket to p5js")]
    [Tooltip("Localhost is your own computer. You don't need to change this.")]
    public string host = "localhost";
    [Tooltip("We connect with p5.serialserver on this port. What is a port? Excellent question, it's kind of like an appartmentnumber in the house that is your computer and p5.serialserver lives there.")]
    public int port = 8081;
    WebSocket websocket;

    int ASCII_LF = 10; // Line Feed
    int ASCII_CR = 13; // Carriage Return
    string message_buffer = "";


  async void requestOpenPort()
  {
      var message = new P5SerialMessage<P5SerialDataSerialPort>();
      message.method = "openserial";
      message.data = new P5SerialDataSerialPort();
      message.data.serialport = serialPort;
      message.data.serialoptions = new P5SerialDataSerialPortOptions();
      string json = JsonUtility.ToJson(message);
      Debug.Log(json);
      await websocket.SendText(json);
  }
  // Start is called before the first frame update
  async void Start()
  {
    // websocket = new WebSocket("ws://echo.websocket.org");
    websocket = new WebSocket("ws://" + host + ":" + port);

    websocket.OnOpen += () =>
    {
      Debug.Log("Connection open!");
      if (openPortAutomatically) {
          requestOpenPort();
      }
    };

    websocket.OnError += (e) =>
    {
      Debug.Log("Error! " + e);
    };

    websocket.OnClose += (e) =>
    {
      Debug.Log("Connection closed!");
    };

    websocket.OnMessage += (bytes) =>
    {
      string json = System.Text.Encoding.UTF8.GetString(bytes);
      //Debug.Log("Received OnMessage! (" + bytes.Length + " bytes) " + json);

      if (json.Contains("\"method\":\"data\"")) {
        P5SerialMessage<int> message = JsonUtility.FromJson<P5SerialMessage<int>>(json);
        char character = (char) message.data;
        if (message.data == ASCII_LF) {
            // line ending, well, whatever
        } else if (message.data == ASCII_CR) {
          int number;
          bool isNumeric = int.TryParse(message_buffer, out number);

          if (isNumeric) {
              //Debug.Log("is numeric " + message_buffer + " (" + number + ")");
            BroadcastMessage("ReceiveMessage", number, SendMessageOptions.DontRequireReceiver);
          } else {
              //Debug.Log("is string " + message_buffer);
            BroadcastMessage("ReceiveMessage", message_buffer, SendMessageOptions.DontRequireReceiver);
          }
          message_buffer = "";
        } else {
          message_buffer += character;
        }
      }
    };

    // Keep sending messages at every 0.3s
    // or not! :-)
    //InvokeRepeating("SendWebSocketMessage", 0.0f, 0.3f);

    await websocket.Connect();
  }

  void Update()
  {
    #if !UNITY_WEBGL || UNITY_EDITOR
      websocket.DispatchMessageQueue();
    #endif
  }

  async void SendWebSocketMessage()
  {
    if (websocket.State == WebSocketState.Open)
    {
      // Sending bytes
      await websocket.Send(new byte[] { 10, 20, 30 });

      // Sending plain text
      await websocket.SendText("plain text message");
    }
  }

  private async void OnApplicationQuit()
  {
    await websocket.Close();
  }
}
