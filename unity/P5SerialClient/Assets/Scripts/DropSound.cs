using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSound : MonoBehaviour
{
    public AudioClip sound;
    public float amplitude;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
        audioSource.clip = sound;
    }


     void OnCollisionEnter (Collision collision)  //Plays Sound Whenever collision detected
     {
         float magnitude = collision.relativeVelocity.magnitude;
         audioSource.volume = magnitude * magnitude * amplitude;
         float pitch = Random.Range(0.6f,1.3f);
         audioSource.pitch = pitch;
         audioSource.Play ();
     }

    // Update is called once per frame
    void Update()
    {

    }
}
