using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveOnOffSkyTint : MonoBehaviour
{
    public Material skyMaterial;
    public float speed = 1.0f;
    public Color onColor = Color.red;
    public Color offColor = Color.black;
    bool isOn = false;
    float lerp = 0.0f;
    // Start is called before the first frame update
    void Start()
    {

       // Call SetColor using the shader property name "_Color" and setting the color to red
       skyMaterial.SetColor("_SkyTint", offColor);

    }

    // Update is called once per frame
    void Update()
    {
        if (isOn) {
            lerp = lerp * (1.0f - speed) + 1.0f * speed;
        } else {
            lerp = lerp * (1.0f - speed) + 0.0f * speed;
        }
        Color color = Color.Lerp(offColor, onColor, lerp);
        skyMaterial.SetColor("_SkyTint", color);
    }

    void ReceiveMessage(string message)
    {
        if (message == "on") {
            isOn = true;
        } else if (message == "off") {
            isOn = false;
        }
    }
    void ReceiveMessage(int message)
    {
        if (message == 1) {
            isOn = true;
        } else if (message == 0) {
            isOn = false;
        }
    }
}
