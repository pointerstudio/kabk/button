from machine import Pin
import time

led = Pin(25, Pin.OUT)
button = Pin(3, Pin.IN, Pin.PULL_UP)
ON_VALUE = False
buttonState = False

while True:
    newButtonState = False
    if button.value() == ON_VALUE:
        print("on")
        newButtonState 
        led.on()
    else:
        print("off")
        led.off()
    time.sleep(0.1)