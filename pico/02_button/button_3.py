from machine import Pin
import time

# this example will send an update to p5.serialserver
# whenever the button value changes

# the internal LED can be used to give some visual feedback
# so we know it works
led = Pin(25, Pin.OUT)

# connect button to GND and Pin 3
# then tell your pico, that you have it connected
button = Pin(3, Pin.IN, Pin.PULL_UP)

# to know if the value changed, we should save
# the last value in a variable
# while we're at this, let's immediately
# put our button value in this variable
# so we know the initial state
last_value = button.value()

# but, how are we going to tell p5.serialserver about our beautiful value?
# we could use a function
# this is useful, if you are lazy and don't want to write the same code again and again
# we can call this function "update" and use it to send the update
def update(value, led):
    # check, if the button is pressed
    # when the button is pressed, the value is "False"
    # this seems unintuitive, but that is how it is
    if value == False:
        # print("blabla") or print(123) sends a message to p5.serialserver
        # don't forget the quotes for text, and don't use the quotes for numbers
        print(1)
        # FYI, could also have been something like
        # print("on")
        
        # then, let's turn the LED on
        led.on()
    else:
        print(0)
        # FYI, could also have been something like
        # print("off")
        
        # then, let's turn the LED off
        led.off()

# now, that we have our function defined.. let's proudly use it
# to send our initial state to p5.serialserver
update(last_value, led)

# and then do it FOREVER
# fun side question: why does this run forever?
while True:
    
    # first, let's get our button value again
    current_value = button.value()
    
    # only do something if the value changed
    # a.k.a is different than last_value
    if current_value is not last_value:
        # yep, it is different. so let's update
        # ... with our fantastic function from before
        update(current_value, led)
        # we can/should use last_value to remember
        # about the current state of our button
        last_value = current_value
        
    # always sleep a little bit
    # microcontrollers are crazy fast
    time.sleep(0.1)
