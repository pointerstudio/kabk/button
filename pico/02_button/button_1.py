from machine import Pin
import time

led = Pin(25, Pin.OUT)
button = Pin(3, Pin.IN, Pin.PULL_UP)
ON_VALUE = False

while True:
    if button.value() == ON_VALUE:
        print("on")
        led.on()
    else:
        print("off")
        led.off()
    time.sleep(0.1)