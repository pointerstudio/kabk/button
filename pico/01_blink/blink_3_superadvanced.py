from machine import Pin, Timer

led = Pin(25, Pin.OUT)
ledState = 0
tim = Timer()
def tick(timer):
    global led
    global ledState
    if ledState == 0:
        ledState = 1
        print("on")
    else:
        ledState = 0
        print("off")
    led.value(ledState)

tim.init(freq=2.5, mode=Timer.PERIODIC, callback=tick)
